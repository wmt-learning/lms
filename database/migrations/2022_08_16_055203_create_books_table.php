<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('name', 100);
            $table->foreignId('category_id')->constrained('book_categories')->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('author_id')->nullabel()->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('publisher_id')->nullabel()->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->string('short_description',300)->nullable();
            $table->string('added_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
};
