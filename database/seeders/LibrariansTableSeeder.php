<?php

namespace Database\Seeders;

use App\Models\Librarian;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LibrariansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Librarian::create([
            "name" => "Willie Nichols",
            "email"=> "willie@ilibrary.com",
            "phone" => 9425251212,
            "password" => "Willie@123",
        ]);
    }
}
