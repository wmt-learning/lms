<?php

use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BookCategoryController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\BookIssueController;
use App\Http\Controllers\PublisherController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/login', function(){
    return "Login";
})->name('login');
Route::get('/librarian/login',[AuthController::class,'showLibrarianLoginForm']);
Route::post('/librarian/login',[AuthController::class,'librarianLogin'])->name('librarian.login');

Route::middleware(['auth:librarian'])->prefix('librarian')->name('librarian.')->group( function (){
    Route::resource('/category', BookCategoryController::class);
    Route::resource('/author', AuthorController::class);
    Route::resource('/publisher', PublisherController::class);
    Route::resource('/book/issue', BookIssueController::class,[
        'as' => 'book'
    ]);
    Route::resource('/book', BookController::class);
    Route::resource('/user', UserController::class);

});

Route::get('/logout',[AuthController::class,'logout'])->name('logout');

//

