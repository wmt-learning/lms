@extends('layouts.master')
@section('title','Manage Books | iLibrary')
@section('venderCSS')
    <link rel="stylesheet" href="{{ asset( 'assets/vendor/css/pages/jquery.dataTables.min.css')}}" />
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Book /</span> Manage Book</h4>
    @php
        $i = 1;
    @endphp
    <!-- Table -->
    <div class="card">
        <h5 class="card-header">Books</h5>
        <div class="table-responsive text-nowrap">
          <table class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Book ID</th>
                <th>Book Name</th>
                <th>Author</th>
                <th>Publisher</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">

               @forelse ($books as $book)
                <tr>
                    <td>{{$i++}}</td>
                    <td>{{$book->id}}</td>
                    <td>{{$book->name}}</td>
                    <td>{{$book->author->name}}</td>
                    <td>{{$book->publisher->name}}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" onclick="editBook({{$book->id}})">Edit</button>
                        <a href="javascript:void(0)" id="delete-book" data-id="{{ $book->id }}" class="btn btn-outline-danger">Delete</a>
                    </td>
                </tr>
               @empty
                <tr>No books...!</tr>
               @endforelse

            </tbody>
          </table>
        </div>
    </div>
    <!--/Table -->
    <div class="row">
        <div class="col">

          <div class="demo-inline-spacing">
            <!-- Basic Pagination -->
            <nav aria-label="Page navigation">

              {{$books->links('pagination::bootstrap-4')}}
            </nav>
            <!--/ Basic Pagination -->
          </div>
        </div>
      </div>
  </div>

@endsection

@section('js')
<script>

    function editBook(id) {
        location.href = "/librarian/book/"+id+"/edit";
    }
    $('body').on('click', '#delete-book', function () {
      var id = $(this).data("id");
      confirm("Are you sure want to delete !");
      $.ajax({
          url: "/librarian/book/"+id,
          data: {"_token": "{{ csrf_token() }}",},
          type: "DELETE",
          success: (response)=>{
              if(response.status == true){
                  $("#row-"+id).hide();
                  $("#msg").html(response.msg);
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
          },
        });
    });

  </script>
@endsection
