@extends('layouts.master')
@section('title','Create Book | iLibrary')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Book /</span> Create Book</h4>

    <!-- Basic Layout -->
    <div class="row">
      <div class="col-xl">
        <div class="card mb-4">
          <div class="card-header d-flex justify-content-between align-items-center">
            <h5 id="msg" class="mb-0">Fill Details</h5>

          </div>
          <div class="card-body">
            <form id="bookForm" name="bookForm" >
              @csrf
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Name</label>
                <input type="text" class="form-control" id="title" name="name" placeholder="Enter a name" />
                <div id="invalid-name" class="invalid-feedback">
                    Please enter a name.
                </div>
              </div>
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Category</label>
                <select class="form-control" id="category_id" name="category_id">
                    @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach
                </select>
                <div id="invalid-category" class="invalid-feedback">
                    Select any category.
                </div>
              </div>
              <div class="mb-3">
                <label class="form-label" for="basic-default-phone">Author</label>
                <select class="form-control" id="author" name="author_id">
                    @foreach ($authors as $author)
                        <option value="{{$author->id}}">{{$author->name}}</option>
                    @endforeach
                </select>
                <div id="invalid-author" class="invalid-feedback">
                    Select any author.
                </div>
              </div>

              <div class="mb-3">
                <label class="form-label" for="basic-default-phone">Publisher</label>
                <select class="form-control" id="publisher_id" name="publisher_id">
                    @foreach ($publishers as $publisher)
                        <option value="{{$publisher->id}}">{{$publisher->name}}</option>
                    @endforeach
                </select>
                <div id="invalid-publisher" class="invalid-feedback">
                    Select any publisher.
                </div>
              </div>

              <div class="mb-3">
                <label class="form-label" for="basic-default-company">Short Discription</label>
                <input type="text" class="form-control" id="short-description" name="short_description" placeholder="Enter Short Discription (Max: 120 Character)" />
                <div id="invalid-short-description" class="invalid-feedback">
                    Enter Short Discription
                </div>
              </div>


              <button type="submit" class="btn btn-primary">Create Book</button>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection
@section('js')
<script>
  $("#name").on('input', function(e){
      $("#invalid-name").hide();
  });
  $("#category_id").on('change', function(e){
      $("#invalid-category").hide();
  });
  $("#author_id").on('change', function(e){
      $("#invalid-author").hide();
  });
  $("#publisher_id").on('change', function(e){
      $("#invalid-publisher").hide();
  });
  $("#short-description").on('input', function(e){
      $("#invalid-short-description").hide();
  });


  $("#bookForm").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      $.ajax({
          url: "{{route('librarian.book.store')}}",
          data: formData,
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          success: (response)=>{
              if(response.status == true){
                  $("#msg").html(response.msg);
                  $(this)[0].reset();
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
              if (response.responseJSON.errors.name) {
                  $("#invalid-name").show();
              }
              if (response.responseJSON.errors.category_id){
                  $("#invalid-category").show();
              }
              if (response.responseJSON.errors.author_id){
                  $("#invalid-author").show();
              }
              if (response.responseJSON.errors.publisher_id){
                  $("#invalid-publisher").show();
              }
              if (response.responseJSON.errors.short_description){
                  $("#invalid-short-description").show();
              }

          },
      });
  });
</script>
@endsection
