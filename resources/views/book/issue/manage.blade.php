@extends('layouts.master')
@section('title','Manage Issue Books | iLibrary')
@section('venderCSS')
    <link rel="stylesheet" href="{{ asset( 'assets/vendor/css/pages/jquery.dataTables.min.css')}}" />
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Issue Book /</span> Manage Issue Book</h4>
    @php
        $i = 1;
    @endphp
    <!-- Table -->
    <div class="card">
        <h5 class="card-header">Issue Books</h5>
        <p id="msg"></p>
        <div class="table-responsive text-nowrap">
          <table class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>User Institude ID</th>
                <th>Issue Book Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">

               @forelse ($issue_books as $issue_book)
                <tr id="row-{{$issue_book->id}}">
                    <td>{{$i++}}</td>
                    <td>{{$issue_book->user->institute_id}}</td>
                    <td>{{$issue_book->book->name}}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" onclick="returnIssueBook({{$issue_book->id}})">Return</button>
                        <a href="javascript:void(0)" id="delete-book" data-id="{{ $issue_book->id }}" class="btn btn-outline-danger">Delete</a>
                    </td>
                </tr>
               @empty
                <tr colspan="4">No Isuue books...!</tr>
               @endforelse

            </tbody>
          </table>
        </div>
    </div>
    <!--/Table -->
    <div class="row">
        <div class="col">

          <div class="demo-inline-spacing">
            <!-- Basic Pagination -->
            <nav aria-label="Page navigation">

              {{$issue_books->links('pagination::bootstrap-4')}}
            </nav>
            <!--/ Basic Pagination -->
          </div>
        </div>
      </div>
  </div>
  <div class="modal fade" id="basicModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel1">Delete</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">

            <strong>Are you sure ?</strong><br>
            You will enable to retrieve data after delete...!
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancle</button>
          <form name="deleteIssueBook" id="deleteIssueBook">
            <input type="hidden" name="book-id" id="book-id" value="">
            <button type="button" class="btn btn-danger">Yes, Delete</button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('js')
<script>

    function returnIssueBook(id) {
        location.href = "/librarian/book/issue/"+id+"/edit";
    }

    $('body').on('click', '#delete-book', function () {

      var id = $(this).data("id");
      confirm("Are you sure want to delete !");
      $.ajax({
          url: "/librarian/book/issue/"+id,
          data: {"_token": "{{ csrf_token() }}", "id": id},
          type: "DELETE",
          success: (response)=>{

              if(response.status == true){
                  $("#row-"+id).hide();
                  $("#msg").html(response.msg);
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
          },
        });
    });

  </script>
@endsection
