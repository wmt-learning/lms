@extends('layouts.master')
@section('title','Issue Book | iLibrary')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Book /</span> Issue Book</h4>

    <!-- Basic Layout -->
    <div class="row">
      <div class="col-xl">
        <div class="card mb-4">
          <div class="card-header d-flex justify-content-between align-items-center">
            <h5 id="msg" class="mb-0">Fill Details</h5>

          </div>
          <div class="card-body">
            <form id="issueBookForm" name="issueBookForm">
                
              @csrf
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Enroll no / Emp code</label>
                <input type="text" class="form-control" id="institute_id" name="institute_id" placeholder="Enter an enroll no / emp code" />
                <div id="invalid-institute-id" class="invalid-feedback">

                </div>
              </div>
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Book Id</label>
                <input type="text" class="form-control" id="book_id" name="book_id" placeholder="Enter a book id" />
                <div id="invalid-book-id" class="invalid-feedback">

                </div>
              </div>
              <button type="submit" class="btn btn-primary">Issue Book</button>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection
@section('js')
<script>
  $("#institute_id").on('input', function(e){
      $("#invalid-institute-id").hide();
  });
  $("#book_id").on('input', function(e){
      $("#invalid-book-id").hide();
  });



  $("#issueBookForm").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      $.ajax({
          url: "{{route('librarian.book.issue.store')}}",
          data: formData,
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          success: (response)=>{
              if(response.status == true){
                  $("#msg").html(response.msg);
                  $(this)[0].reset();
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
              if (response.responseJSON.errors.book_id) {
                  $("#invalid-book-id").html(response.responseJSON.errors.book_id);
                  $("#invalid-book-id").show();
              }
              if (response.responseJSON.errors.institute_id){
                  $("#invalid-institute-id").html(response.responseJSON.errors.institute_id);
                  $("#invalid-institute-id").show();
              }

          },
      });
  });
</script>
@endsection
