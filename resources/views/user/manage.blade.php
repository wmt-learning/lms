@extends('layouts.master')
@section('title','Manage Users | iLibrary')
@section('venderCSS')
    <link rel="stylesheet" href="{{ asset( 'assets/vendor/css/pages/jquery.dataTables.min.css')}}" />
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">User /</span> Manage User</h4>
    @php
        $i = 1;
    @endphp
    <!-- Table -->

    <div class="card">
        <h5 class="card-header">Users</h5>
        <p id="msg"></p>
        <div class="table-responsive text-nowrap">
          <table class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>User Name</th>
                <th>Email</th>
                <th>Institude ID</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">

               @forelse ($users as $user)
                <tr id="row-{{$user->id}}">
                    <td>{{$i++}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->institute_id}}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" onclick="editUser({{$user->id}})">Edit</button>
                        <a href="javascript:void(0)" id="delete-user" data-id="{{ $user->id }}" class="btn btn-outline-danger">Delete</a>

                    </td>
                </tr>
               @empty
                <tr>No books...!</tr>
               @endforelse

            </tbody>
          </table>
        </div>
    </div>
    <!--/Table -->
    <div class="row">
        <div class="col">

          <div class="demo-inline-spacing">
            <!-- Basic Pagination -->
            <nav aria-label="Page navigation">

              {{$users->links('pagination::bootstrap-4')}}
            </nav>
            <!--/ Basic Pagination -->
          </div>
        </div>
      </div>
  </div>



@endsection

@section('js')
  <script>

    function editUser(id) {
        location.href = "/librarian/user/"+id+"/edit";
    }

    $('body').on('click', '#delete-user', function () {
      var id = $(this).data("id");
      confirm("Are you sure want to delete !");
      $.ajax({
          url: "/librarian/user/"+id,
          data: {"_token": "{{ csrf_token() }}",},
          type: "DELETE",
          success: (response)=>{
              if(response.status == true){
                  $("#row-"+id).hide();
                  $("#msg").html(response.msg);
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
          },
        });
    });



  </script>
@endsection

