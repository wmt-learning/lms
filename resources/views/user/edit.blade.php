@extends('layouts.master')
@section('title','Create User | iLibrary')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">User /</span> Edit User</h4>

    <!-- Basic Layout -->
    <div class="row">
      <div class="col-xl">
        <div class="card mb-4">
          <div class="card-header d-flex justify-content-between align-items-center">
            <h5 id="msg" class="mb-0">Fill Details</h5>

          </div>
          <div class="card-body">
            <form id="userForm" name="userForm" >
              @method('PATCH')
              @csrf
              <input type="hidden" name="user_id" id="user-id" value="{{$user->id}}">
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Name</label>
                <input type="text" class="form-control" id="title" name="name" placeholder="Enter a name" value="{{$user->name}}"/>
                <div id="invalid-name" class="invalid-feedback">
                    Please enter a name.
                </div>
              </div>
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Enter a email" value="{{$user->email}}"/>
                <div id="invalid-email" class="invalid-feedback">
                    Please enter an email.
                </div>
              </div>

              <div class="mb-3">
                <label class="form-label" for="basic-default-institute_id">Enroll No. / Emp Code</label>
                <input type="text" class="form-control" id="institute_id" name="institute_id" placeholder="Enter a enroll / emp code" value="{{$user->institute_id}}"/>
                <div id="invalid-institute-id" class="invalid-feedback">
                    Please enter an enroll no / emp code.
                </div>
              </div>

              <div class="mb-3">
                <label class="form-label" for="basic-default-phone">Password</label>
                <input type="text" class="form-control" id="password" name="password" placeholder="Enter a enroll / emp code" />
                <div id="invalid-password" class="invalid-feedback">
                    Please enter a password.
                </div>

              </div>

              <div class="mb-3">

                <input class="form-check-input" type="checkbox" value="1" name="is_employee" id="is_employee" @if ($user->is_employee)
                    @checked(true)
                @endif />
                <label class="form-check-label" for="is_employee">
                    is Emmployee ?
                </label>
                <div id="invalid-short-description" class="invalid-feedback">
                    Enter Short Discription
                </div>
              </div>


              <button type="submit" class="btn btn-primary">Update User</button>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection
@section('js')
<script>
  $("#name").on('input', function(e){
      $("#invalid-name").hide();
  });
  $("#email").on('input', function(e){
      $("#invalid-email").hide();
  });
  $("#institute_id").on('input', function(e){
      $("#invalid-institute-id").hide();
  });
  $("#password").on('input', function(e){
      $("#invalid-password").hide();
  });



  $("#userForm").submit(function (e) {
      e.preventDefault();
      var id = $("#user-id").val();
      var formData = new FormData(this);
      $.ajax({
          url: "/librarian/user/"+id,
          data: formData,
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          success: (response)=>{
              if(response.status == true){
                  $("#msg").html(response.msg);
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
              if (response.responseJSON.errors.name) {
                  $("#invalid-name").show();
              }
              if (response.responseJSON.errors.email){
                  $("#invalid-email").show();
              }
              if (response.responseJSON.errors.institute_id){
                  $("#invalid-institute-id").show();
              }
              if (response.responseJSON.errors.password){
                  $("#invalid-password").show();
              }
          },
      });
  });
</script>
@endsection
