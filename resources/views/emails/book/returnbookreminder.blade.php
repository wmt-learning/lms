@component('mail::message')
Dear {{$name}},

Some of Books you have borrowed from library
@if ($days > 15)
    are overdue. Please return books & You have pays 10 Rupees panlty
@else
    will overdue in some days. Please return book before 15 days time limits. Otherwise you will have to pay penalty.
@endif

This is auto generate mail. Don't reply.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
