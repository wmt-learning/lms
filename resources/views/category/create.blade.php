@extends('layouts.master')
@section('title','Create Category | iLibrary')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Category /</span> Create Category</h4>
    <!-- Basic Layout -->
    <div class="row">
      <div class="col-xl">
        <div class="card mb-4">
          <div class="card-header d-flex justify-content-between align-items-center">
            <h5 id="msg" class="mb-0">Fill Details</h5>
          </div>
          <div class="card-body">
            <form id="categoryForm" name="categoryForm">
              @csrf
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter a name" />
                <div id="invalid-name" class="invalid-feedback">
                    Please enter a name.
                </div>
              </div>
              <button type="submit" class="btn btn-primary">Create Category</button>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection
@section('js')
<script>
  $("#name").on('input', function(e){
      $("#invalid-name").hide();
  });

  $("#categoryForm").submit(function (e) {
      e.preventDefault();

      var formData = new FormData(this);

      $.ajax({
          url: "{{route('librarian.category.store')}}",
          data: formData,
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          success: (response)=>{
              if(response.status == true){
                  $("#msg").html(response.msg);
                  $(this)[0].reset();
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
              if (response.responseJSON.errors.name) {
                  $("#invalid-name").html(response.responseJSON.errors.name);
                  $("#invalid-name").show();
              }

          },
      });
  });
</script>
@endsection
