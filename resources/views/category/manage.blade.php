@extends('layouts.master')
@section('title','Manage Categories | iLibrary')
@section('venderCSS')
    <link rel="stylesheet" href="{{ asset( 'assets/vendor/css/pages/jquery.dataTables.min.css')}}" />
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Category /</span> Manage Category</h4>
    @php
        $i = 1;
    @endphp
    <!-- Table -->

    <div class="card">
        <h5 class="card-header">Categories</h5>
        <p id="msg"></p>
        <div class="table-responsive text-nowrap">
          <table class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Category Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">

               @forelse ($categories as $category)
                <tr id="row-{{$category->id}}">
                    <td>{{$i++}}</td>
                    <td>{{$category->name}}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" onclick="editCategory({{$category->id}})">Edit</button>
                        <a href="javascript:void(0)" id="delete-category" data-id="{{ $category->id }}" class="btn btn-outline-danger">Delete</a>
                    </td>
                </tr>
               @empty
                <tr>No Categories...!</tr>
               @endforelse

            </tbody>
          </table>
        </div>
    </div>
    <!--/Table -->
    <div class="row">
        <div class="col">

          <div class="demo-inline-spacing">
            <!-- Basic Pagination -->
            <nav aria-label="Page navigation">

              {{$categories->links('pagination::bootstrap-4')}}
            </nav>
            <!--/ Basic Pagination -->
          </div>
        </div>
      </div>
  </div>



@endsection

@section('js')
  <script>

    function editCategory(id) {
        location.href = "/librarian/category/"+id+"/edit";
    }
    $('body').on('click', '#delete-category', function () {
      var id = $(this).data("id");
      confirm("Are You sure want to delete !");
      $.ajax({
          url: "/librarian/category/"+id,
          data: {"_token": "{{ csrf_token() }}",},
          type: "DELETE",
          success: (response)=>{
              if(response.status == true){
                  $("#row-"+id).hide();
                  $("#msg").html(response.msg);
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
          },
        });
    });



  </script>
@endsection

