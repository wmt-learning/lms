@extends('layouts.master')
@section('title','Create Author | iLibrary')
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Author /</span> Edit Auhor</h4>

    <!-- Basic Layout -->
    <div class="row">
      <div class="col-xl">
        <div class="card mb-4">
          <div class="card-header d-flex justify-content-between align-items-center">
            <h5 id="msg" class="mb-0">Fill Details</h5>
          </div>
          <div class="card-body">
            <form id="authorForm" name="authorForm">
              @method('PATCH')
              @csrf
              <input type="hidden" name="author_id" id="author-id" value="{{$author->id}}">
              <div class="mb-3">
                <label class="form-label" for="basic-default-fullname">Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Enter a name" value="{{$author->name}}"/>
                <div id="invalid-name" class="invalid-feedback">
                    Please enter a name.
                </div>
              </div>
              <button type="submit" class="btn btn-primary">Update Author</button>
            </form>
          </div>
        </div>
      </div>

    </div>
  </div>
@endsection
@section('js')
<script>
  $("#name").on('input', function(e){
      $("#invalid-name").hide();
  });

  $("#authorForm").submit(function (e) {
      e.preventDefault();
      var id = $("#author-id").val();
      var formData = new FormData(this);

      $.ajax({
          url: "/librarian/author/"+id,
          data: formData,
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          success: (response)=>{
              if(response.status == true){
                  $("#msg").html(response.msg);
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
              if (response.responseJSON.errors.name) {
                  $("#invalid-name").html(response.responseJSON.errors.name);
                  $("#invalid-name").show();
              }

          },
      });
  });
</script>
@endsection
