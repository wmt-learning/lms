@extends('layouts.master')
@section('title','Manage Authors | iLibrary')
@section('venderCSS')
    <link rel="stylesheet" href="{{ asset( 'assets/vendor/css/pages/jquery.dataTables.min.css')}}" />
@endsection
@section('content')
<div class="container-xxl flex-grow-1 container-p-y">
    <h4 class="fw-bold py-3 mb-4"><span class="text-muted fw-light">Author /</span> Manage Author</h4>
    @php
        $i = 1;
    @endphp
    <!-- Table -->

    <div class="card">
        <h5 class="card-header">Authors</h5>
        <p id="msg"></p>
        <div class="table-responsive text-nowrap">
          <table class="table">
            <thead>
              <tr>
                <th>No.</th>
                <th>Author Name</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody class="table-border-bottom-0">

               @forelse ($authors as $author)
                <tr id="row-{{$author->id}}">
                    <td>{{$i++}}</td>
                    <td>{{$author->name}}</td>
                    <td>
                        <button type="button" class="btn btn-outline-primary" onclick="editAuthor({{$author->id}})">Edit</button>
                        <a href="javascript:void(0)" id="delete-author" data-id="{{ $author->id }}" class="btn btn-outline-danger">Delete</a>

                    </td>
                </tr>
               @empty
                <tr>No Authors...!</tr>
               @endforelse

            </tbody>
          </table>
        </div>
    </div>
    <!--/Table -->
    <div class="row">
        <div class="col">

          <div class="demo-inline-spacing">
            <!-- Basic Pagination -->
            <nav aria-label="Page navigation">

              {{$authors->links('pagination::bootstrap-4')}}
            </nav>
            <!--/ Basic Pagination -->
          </div>
        </div>
      </div>
  </div>



@endsection

@section('js')
  <script>

    function editAuthor(id) {
        location.href = "/librarian/author/"+id+"/edit";
    }

    $('body').on('click', '#delete-author', function () {
      var id = $(this).data("id");
      confirm("Are you sure want to delete !");
      $.ajax({
          url: "/librarian/author/"+id,
          data: {"_token": "{{ csrf_token() }}",},
          type: "DELETE",

          success: (response)=>{
              if(response.status == true){
                  $("#row-"+id).hide();
                  $("#msg").html(response.msg);
                  $(this)[0].reset();
              }
              else{
                  $("#msg").html(response.msg);
              }
          },
          error: (response)=>{
              console.log(response);
          },
        });
    });




  </script>
@endsection

