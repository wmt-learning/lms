<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Book extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'category_id', 'author_id', 'publisher_id' , 'short_description' ,'added_by'
    ];

    public function category()
    {
        return $this->belongsTo(BookCategory::class);
    }

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    public function publisher()
    {
        return $this->belongsTo(Publisher::class);
    }



}
