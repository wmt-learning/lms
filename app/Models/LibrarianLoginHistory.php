<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LibrarianLoginHistory extends Model
{
    use HasFactory;
    protected $table = "librarian_login_histories";
    protected $fillable = [
        'librarian','ip_address'
    ];

}
