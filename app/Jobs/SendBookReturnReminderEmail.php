<?php

namespace App\Jobs;

use App\Mail\ReturnBookReminderMail;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendBookReturnReminderEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $book_issues;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($book_issues)
    {
        $this->book_issues = $book_issues;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->book_issues->each(function ($book_issue){
            $days = now()->diffInDays(Carbon::parse($book_issue->issued_at));
            if($days > 9){
                Mail::to($book_issue->user->email)->send(new ReturnBookReminderMail($book_issue,$days));
            }
        });
    }
}
