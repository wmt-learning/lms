<?php

namespace App\Console\Commands;

use App\Jobs\SendBookReturnReminderEmail;
use App\Models\BookIssue;
use Illuminate\Console\Command;

class SendReturnBookReminderEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mail:return-book-reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder mail for return book';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $book_issues = BookIssue::where('returned_at', null)->get();

        // SendBookReturnReminderEmail::dispatch($book_issues);
        dispatch(new SendBookReturnReminderEmail($book_issues));
        return "Reminder Mail Job is Ready";
    }
}
