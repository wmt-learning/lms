<?php

namespace App\Http\Controllers;

use App\Models\BookCategory;
use Illuminate\Http\Request;

class BookCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = BookCategory::paginate(10,['id','name']);
        return view('category.manage',compact(["categories"]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => ['required','string','min:3','unique:book_categories,name'],
        ]);
        BookCategory::create(['name' => $request->name,]);
        return response()->json(["status" => true, "msg" => "Category added successfully...!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BookCategory $book_category)
    {
        return $book_category;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book_category = BookCategory::find($id);
        return view('category.edit', compact(['book_category']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => ['required','string','min:3','unique:book_categories,name,'.$id],
        ]);
        BookCategory::where('id',$id)->update(["name"=>$request->name]);
        return response()->json(["status" => true, "msg" => "Category update successfully...!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book_category = BookCategory::find($id);
        $book_category->delete();
        return response()->json(["status" => true, "msg" => "Category data deleted...!"]);
    }
}
