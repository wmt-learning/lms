<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(1,['id','name','email','institute_id']);;
        return view('user.manage',compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'name' => ['required','string','min:5'],
            'email'=> ['required','email','unique:users,email'],
            'institute_id' => ['required','string','min:5','unique:users,institude_id'],
            'password' => ['required','string'],
            'is_employee' => ['nullable'],
        ]);
        User::create($validate);
        return response()->json(["status" => true, "msg" => "User added successfully...!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit',compact(['user']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request,[
            'name' => ['required','string','min:5'],
            'email'=> ['required','email','unique:user,email,'.$id],
            'institute_id' => ['required','string','min:5','unique:users,institude_id,'.$id],
            'password' => ['nullable','string'],
            'is_employee' => ['nullable'],
        ]);
        if ($request->password) {
            User::where("id",$id)->update($validate);
        }else{
            User::where("id",$id)->update([
                'name' => $request->name,
                'email'=> $request->email,
                'institute_id' => $request->institude_id,
                'is_employee' => $request->is_employee,
            ]);
        }
        return response()->json(["status" => true, "msg" => "User data updated successfully...!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(["status" => true, "msg" => "User data deleted...!"]);
    }
}
