<?php

namespace App\Http\Controllers;

use App\Models\Author;
use App\Models\Book;
use App\Models\BookCategory;
use App\Models\Publisher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with(['author:id,name','publisher:id,name'])->paginate(1,['id','name','author_id','publisher_id']);
        return view('book.manage',compact(['books']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = BookCategory::all(['id','name']);
        $authors = Author::all(['id','name']);
        $publishers = Publisher::all(['id','name']);
        return view('book.create',compact(['categories','authors','publishers']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = $this->validate($request,[
            'name' => ['required', 'string' , 'min:3'],
            'category_id' => ['required','numeric'],
            'author_id' => ['required','numeric'],
            'publisher_id' => ['required','numeric'],
            'short_description' => ['nullable', 'string' , 'min:30'],
        ]);
        $validate["added_by"] = Auth::user()->email;
        Book::create($validate);
        return response()->json(["status" => true, "msg" => "Book added successfully...!"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $book = Book::find($id);
        return $book;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = BookCategory::all(['id','name']);
        $authors = Author::all(['id','name']);
        $publishers = Publisher::all(['id','name']);
        $book = Book::find($id);
        return view('book.edit',compact(['book','categories','authors','publishers']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = $this->validate($request,[
            'name' => ['required', 'string' , 'min:3'],
            'category_id' => ['required','numeric'],
            'author_id' => ['required','numeric'],
            'publisher_id' => ['required','numeric'],
            'short_description' => ['nullable', 'string' , 'min:30'],
        ]);
        $validate["added_by"] = Auth::user()->email;
        Book::where('id',$id)->update($validate);
        return response()->json(["status" => true, "msg" => "Book updated successfully...!"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();
        return response()->json(["status" => true, "msg" => "Book data deleted"]);
    }
}
