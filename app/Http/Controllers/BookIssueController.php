<?php

namespace App\Http\Controllers;

use App\Models\BookIssue;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BookIssueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $issue_books = BookIssue::where('returned_at', null)->with(['user:id,name,institute_id','book:id,name'])->paginate(2,['id','user_id','book_id','issued_at']);
        return view('book.issue.manage',compact(['issue_books']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('book.issue.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'institute_id' => ['required','string','exists:users,institute_id'],
            'book_id' => ['required','string','exists:books,id'],
        ]);
        $user = User::where('institute_id', $request->institute_id)->first();
        $book_issued = BookIssue::where('user_id',$user->id)->where('book_id', $request->book_id)->where('returned_at', null)->get();
        if(count($book_issued) > 0){
            return response()->json(["status" => false, "msg" => "This Book already issued by ".$user->name."...!"]);
        }
        BookIssue::create([
            "user_id" => $user->id,
            "book_id" => $request->book_id,
            "issue_status" => "Issued",
            "authority_id" => Auth::guard('librarian')->user()->id,
        ]);
        return response()->json(["status" => true, "msg" => "Book issued successfully...!"]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book_issue = BookIssue::find($id);
        return view('book.issue.return',compact(['id','book_issue']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'institute_id' => ['required','string','exists:users,institute_id'],
            'book_id' => ['required','string'],
        ]);
        $user = User::where('institute_id', $request->institute_id)->first();
        $book_issue = BookIssue::where('user_id',$user->id)->where('book_id', $request->book_id)->where('returned_at', null)->first();
        if(is_object($book_issue)){
            $book_issue->returned_at = Carbon::now()->toDateTimeString();
            $book_issue->issue_status = "Returned";
            $book_issue->save();
            return response()->json(["status" => true, "msg" => "Book returned successfully...!"]);
        }
        else{
            return response()->json(["status" => false, "msg" => "No record found...!"]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book_issue = BookIssue::find($id);
        $book_issue->delete();
        return response()->json(["status" => true, "msg" => "Book issue data deleted...!"]);
    }
}
