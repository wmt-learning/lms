<?php

namespace App\Http\Controllers\Auth;

use App\Events\LibrarianLoginHistory;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LibrarianLoginRequest;
use App\Models\Librarian;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function showLibrarianLoginForm()
    {
        return view('auth.librarian.login');
    }

    public function librarianLogin(LibrarianLoginRequest $request){
        $librarian = Librarian::where('email',$request->login)->orWhere('phone',$request->login)->first();
        if(!$librarian || !Hash::check($request->password, $librarian->password)){
            throw new HttpResponseException(response()->json(["status" => false, "msg" => "Your creadentials are invalid"]));
        }
        Auth::guard('librarian')->login($librarian,true);
        event(new LibrarianLoginHistory($librarian->email,$request->ip()));
        // $request->authenticate();
        $request->session()->regenerate();
        $librarian = Auth::guard('librarian')->user()->email;
        return response()->json(["status"=>true , "msg" => "You are login successfully"]);
    }

    public function logout(Request $request)
    {
        if(Auth::guard('librarian')->check()){
            Auth::guard('librarian')->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            return redirect('/librarian/login');
        }

    }
}
