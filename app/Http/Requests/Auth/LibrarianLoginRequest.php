<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LibrarianLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'login' => ['required','string'],
            'password' => ['required','string'],
        ];
    }

    // public function authenticate(){
    //     $librarian = Librarian::where('email',$this->login)->orWhere('phone',$this->login)->first();
    //     if(!$librarian || !Hash::check($this->password, $librarian->password)){
    //         throw new HttpResponseException(response()->json(["status" => false, "msg" => "Your creadentials are invalid"]));
    //     }
    //     Auth::guard('librarian')->login($librarian,true);
    //     event(new LibrarianLoginHistory($librarian->email,$this->ip()));
    // }
}
