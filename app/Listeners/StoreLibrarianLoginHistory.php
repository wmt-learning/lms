<?php

namespace App\Listeners;

use App\Events\LibrarianLoginHistory;
use App\Models\LibrarianLoginHistory as ModelsLibrarianLoginHistory;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class StoreLibrarianLoginHistory
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\LibrarianLoginHistory  $event
     * @return void
     */
    public function handle(LibrarianLoginHistory $event)
    {

        $login_history = ModelsLibrarianLoginHistory::create([
            "librarian" => $event->librarian,
            "ip_address" => $event->ip_address,
        ]);
    }
}
