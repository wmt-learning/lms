<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReturnBookReminderMail extends Mailable
{
    use Queueable, SerializesModels;
    public $book_issue,$days;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($book_issue,$days)
    {
        $this->book_issue = $book_issue;
        $this->days = $days;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.book.returnbookreminder')->with([
            'name' => $this->book_issue->user->name,
            'days' => $this->days,
        ]);
    }
}
